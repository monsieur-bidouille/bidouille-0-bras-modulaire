# Bidouille 0 - Bras modulaire

Documentation des Bras articulés décrits dans l'épisode Bidouille N°0.

Lien de la vidéo :

## Bras anglepoise 

Lien de la modélisation sur Fusion 360 : https://a360.co/3dQLZP4

### BOM

* Vis tête hexagonale M5 * 14mm
* Roulements 5 * 11 * 5mm
* Elastique de tendeur au mètre
* Profilé aluminium type 6B 20*20mm longueurs :
  * 480mm * 4 
  * 200mm * 2

## Bras articulé V2 

Lien de la modélisation sur Fusion 360 : https://a360.co/2VyQXtp

### BOM 

* Profilé aluminium type 6B 20*20mm longueur variable en fonction de vos besoins (pour ma part c'est 300mm)
* Vis tête hexagonale M5 * 14mm
* T-nuts M5 
* Tige filetée M8 + écrous.
* Pour la rotule : Vis M6 * 30mm X 3
* Blocage : Vis M8*50mm
* Parties imprimées en 3D en PLA.

NOTE : Un abonné, Philippe, m'a envoyé des photos de sa réalisation et le modèle d'une pièce renforcée (en .STL et .SKP). Merci à lui !